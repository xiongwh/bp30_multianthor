/*! ----------------------------------------------------------------------------
 * @file	port.h
 * @brief	HW specific definitions and functions for portability
 *
 * @attention
 *
 * Copyright 2013 (c) DecaWave Ltd, Dublin, Ireland.
 *
 * All rights reserved.
 *
 * @author DecaWave
 */


#ifndef PORT_H_
#define PORT_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f4xx_hal.h"

/* Define our wanted value of CLOCKS_PER_SEC so that we have a millisecond
 * tick timer. */
#undef  CLOCKS_PER_SEC
#define CLOCKS_PER_SEC 1000

//void printf2(const char *format, ...);

extern int writetospi_serial(uint16_t headerLength,
                             const uint8_t *headerBuffer,
                             uint32_t bodylength,
                             const uint8_t *bodyBuffer);

extern int readfromspi_serial(uint16_t	headerLength,
                              const uint8_t *headerBuffer,
                              uint32_t readlength,
                              uint8_t *readBuffer );

#define writetospi  writetospi_serial
#define readfromspi readfromspi_serial

#define SPIx_PRESCALER				SPI_BaudRatePrescaler_8

#define SPIx						SPI1
#define SPIx_GPIO					GPIOA
#define SPIx_CS						GPIO_PIN_4
#define SPIx_CS_GPIO			GPIOA
#define SPIx_SCK					GPIO_PIN_5
#define SPIx_MISO					GPIO_PIN_6
#define SPIx_MOSI					GPIO_PIN_7

#define DW1000_RSTn					GPIO_PIN_4|GPIO_PIN_10 //PA node,PB10
#define DW1000_RSTn_GPIO			GPIOB

#define DECARSTIRQ                  GPIO_PIN_0
#define DECARSTIRQ_GPIO             GPIOA
#define DECARSTIRQ_EXTI             EXTI_Line0
#define DECARSTIRQ_EXTI_PORT        GPIO_PortSourceGPIOA
#define DECARSTIRQ_EXTI_PIN         GPIO_PINSource0
#define DECARSTIRQ_EXTI_IRQn        EXTI0_IRQn


#define DECAIRQ                     GPIO_PIN_0
#define DECAIRQ_GPIO                GPIOB
#define DECAIRQ_EXTI                EXTI_LineB
#define DECAIRQ_EXTI_PORT           GPIO_PortSourceGPIOB
#define DECAIRQ_EXTI_PIN            GPIO_PINSource0
#define DECAIRQ_EXTI_IRQn           EXTI0_IRQn
#define DECAIRQ_EXTI_USEIRQ         ENABLE
#define DECAIRQ_EXTI_NOIRQ          DISABLE


ITStatus EXTI_GetITEnStatus(uint32_t x);

#define port_GetEXT_IRQStatus()             EXTI_GetITEnStatus(DECAIRQ_EXTI_IRQn)
#define port_DisableEXT_IRQ()               NVIC_DisableIRQ(DECAIRQ_EXTI_IRQn)
#define port_EnableEXT_IRQ()                NVIC_EnableIRQ(DECAIRQ_EXTI_IRQn)
#define port_CheckEXT_IRQ()                 GPIO_ReadInputDataBit(DECAIRQ_GPIO, DECAIRQ)
int NVIC_DisableDECAIRQ(void);


//extern I2C_HandleTypeDef hi2c1;
//extern SPI_HandleTypeDef hspi1;
//extern UART_HandleTypeDef huart1;
void peripherals_init (void);
void SPI_ConfigFastRate(uint16_t scalingfactor);
void spi_set_rate_low (void);
void spi_set_rate_high (void);
unsigned long portGetTickCnt(void);
#define portGetTickCount() 			portGetTickCnt()
void reset_DW1000(void);
void USART_putc(char c);
void Delay_us(int nTimer);
void bphero_setcallbacks(void (*rxcallback)(void));
void USART_puts(uint8_t *s,uint8_t len);

#ifdef __cplusplus
}
#endif

#endif /* PORT_H_ */
